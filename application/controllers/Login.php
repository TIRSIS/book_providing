<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    private $result = array();

    public function index()
    {
        $user_information = $this->session->userdata('userinfo');

        if($this->input->is_ajax_request())
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            if(!empty($username) && !empty($password))
            {
                $result = $this->set_user($username, $password);

                echo json_encode($result);
            }
            else
            {
                $this->result['success'] = FALSE;
                $this->result['message'] = 'Оба поля должны быть заполнены';
            }
        }
        else
        {
            empty($user_information) ? $this->get_authorization_html() : redirect(site_url());
        }
    }

    private function set_user($username, $password)
    {
        $this->load->model('authorization_model');

        $this->result['success'] = TRUE;

        try
        {
            $user = $this->authorization_model->get_user_info($username, $password);

            $user['reg_time'] = date('Y-m-d H:i:s');

            $this->session->set_userdata(array('userinfo' => $user));
        }
        catch(User_auth_exception $exception)
        {
            $this->result['success'] = FALSE;
            $this->result['message'] = $exception->getMessage();
        }

        return $this->result;
    }

    private function get_authorization_html()
    {
        //вызов страницы авторизации
    }
}