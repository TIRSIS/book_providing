<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_auth_exception extends Exception
{
    function __construct($message = null)
    {
        if(isset($message))
        {
            parent::__construct($message);
        }
    }
}

class Authorization_model extends CI_Model
{
    function get_user_info($username, $password)
    {
        $user = $this->db
            ->select('us.id, us.name, us.usertype', FALSE)
            ->where('us.username', $username)
            ->where('us.password', $password)
            ->get('users AS us')->row_array();

        if(!isset($user))
        {
            throw new User_auth_exception('Пользователь не найден');
        }

        return $user;
    }
}