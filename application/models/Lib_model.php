<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Lib_model extends CI_Model
{
    private $lib_db;

    public function __construct()
    {
        parent::__construct();

        $lib_connection_config = array(
            'dsn'	=> '',
            'hostname' => '192.168.11.190',
            'username' => 'root',
            'password' => 'eLibsystem49',
            'database' => 'sup',
            'dbdriver' => 'mysqli',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => (ENVIRONMENT !== 'production'),
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );

        $this->lib_db = $this->load->database($lib_connection_config, TRUE);
    }

    public function get_lib_books($author, $title, $year)
    {
        $this->lib_db
            ->select('
                bk.id_books AS book_id,
                bk.autor_books AS book_author,
                bk.name_books AS book_title,
                bk.year_books AS book_year', FALSE);

        if($author != NULL)
            $this->lib_db->like('bk.autor_books', $author, 'both');

        if($title != NULL)
            $this->lib_db->like('bk.name_books', $title, 'both');

        if($year != NULL)
            $this->lib_db->like('bk.year_books', $year, 'both');

        return $this->lib_db->get('books AS bk')->result_array();
    }
}