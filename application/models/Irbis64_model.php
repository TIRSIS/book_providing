<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Irbis64_exception extends Exception
{
    function __construct($message = null)
    {
        if(isset($message))
        {
            parent::__construct($message);
        }
    }
}

class Irbis64_model extends CI_Model
{
    private
        $username = "1",
        $password = "1",

        $client_id = "1235813",
        $client_type = "C",
        $database = "IBIS",

        $server_ip = "192.168.0.103",
        $server_port = 6666;

    private
        $query_number = 1;

    private $IRBIS64_errors = array(
        0 => "ZERO",
        -1111 => "SERVER_EXECUTE_ERROR",
        -2222 => "WRONG_PROTOCOL",
        -3333 => "CLIENT_NOT_IN_LIST",
        -3334 => "CLIENT_NOT_IN_USE",
        -3335 => "CLIENT_IDENTIFIER_WRONG",
        -3336 => "CLIENT_NOT_ALLOWED",
        -3337 => "CLIENT_ALREADY_EXISTS",
        -4444 => "WRONG_PASSWORD",
        -5555 => "FILE_NOT_EXISTS",
        -6666 => "SERVER_OVERLOAD",
        -7777 => "PROCESS_ERROR",
        -100 => "READ_WRONG_MFN",
        -600 => "REC_DELETE",
        -601 => "REC_PHYS_DELETE",
        -602 => "ERR_RECLOCKED",
        -603 => "REC_DELETE",
        -607 => "AUTOIN_ERROR",
        -300 => "ERR_DBEWLOCK",
        -400 => "ERR_FILEMASTER",
        -401 => "ERR_FILEINVERT",
        -402 => "ERR_WRITE",
        -403 => "ERR_ACTUAL",
        -203 => "TERM_LAST_IN_LIST",
        -204 => "TERM_FIRST_IN_LIST",
        -202 => "TERM_NOT_EXISTS"
    );

    public function __construct()
    {
        $command = "A";

        $query = $command . "\n" .
            $this->client_type . "\n" .
            $command . "\n" .
            $this->client_id . "\n" .
            $this->query_number .
            "\n\n\n\n\n\n".
            $this->username . "\n" .
            $this->password . "\n";

        $this->query_number++;

        $result = $this->execute_command($query);

        if($result[10] != 0 && $result[10] != -3337)
        {
            $this->throw_irbis64_exception($result[10]);
        }
    }

    public function __destruct()
    {
        $command = "B";

        $query = $command ."\n" .
            $this->client_type . "\n" .
            $command . "\n" .
            $this->client_id . "\n" .
            $this->query_number .
            "\n\n\n\n\n\n".
            $this->username . "\n" .
            $this->password;

        $this->execute_command($query);
    }

    //Возрат записи по заданному mfn.
    //Где: $mfn - значение mfn; $block - блокировка записи, по умолчанию 0, т.е. нет.
    public function get_record_by_mfn($mfn, $block = 0)
    {
        $command = "C";

        $query = $command."\n".
            $this->client_type."\n".
            $command."\n".
            $this->client_id."\n".
            $this->query_number."\n".
            $this->username."\n".
            $this->password."\n".
            "\n\n\n".
            $this->database."\n".
            $mfn."\n".
            $block."\n";

        $this->query_number++;
        $result = $this->execute_command($query);

        if($result[10] != 0)
        {
            $this->throw_irbis64_exception($result[10]);
        }

        $record = array();

        $index = 13;

        while(isset($result[$index]) && !empty($result[$index]))
        {
            $division = strpos($result[$index], '#');

            $split_field_by_number = array(
                mb_substr($result[$index], 0, $division),
                mb_substr($result[$index], $division)
            );

            $split_field_by_letter = explode('^', $split_field_by_number[1]);

            $sub_fields = array();

            if(count($split_field_by_letter) != 1)
            {
                foreach($split_field_by_letter as $sfl)
                {
                    $sub_field_letter = mb_substr($sfl, 0, 1);

                    $sub_fields[$sub_field_letter] = mb_substr($sfl, 1);

                    if(empty($sub_fields[$sub_field_letter]))
                    {
                        unset($sub_fields[$sub_field_letter]);
                    }
                }
            }
            else
            {
                $sub_fields = mb_substr($split_field_by_letter[0], 1);
            }

            $field_number = $split_field_by_number[0];

            $record[$field_number] = $sub_fields;

            unset($sub_fields);

            $index++;
        }

        return $record;
    }

    //Поиск записи по заданному поисковому выражению.
    //Где: $search_expression - поисковое выражение;
    //  $record_numbers - кол-во возращаемых записей, по умолчанию 0, т.е. все;
    //  $first_record - номер первой возвращаемой записи в общем списке найденных записей;
    //  $format - форматирование возращаемой записи, по умолчанию форматирование не применяется.
    public function search_mfn_by_request($search_expression, $record_numbers = 0, $first_record = 1, $format = "")
    {
        $command = "K";

        $query = $command."\n".
            $this->client_type."\n".
            $command."\n".
            $this->client_id."\n".
            $this->query_number."\n".
            $this->username."\n".
            $this->password."\n".
            "\n\n\n".
            $this->database."\n".
            $search_expression."\n".
            $record_numbers."\n".
            $first_record."\n".
            $format."\n";

        $this->query_number++;
        $result = $this->execute_command($query);

        if($result[10] != 0)
        {
            $this->throw_irbis64_exception($result[10]);
        }

        $mfn = array();

        $index = 12;

        while(isset($result[$index]) && !empty($result[$index]))
        {
            $mfn[] = $result[$index];

            $index++;
        }

        return $mfn;
    }

    private function execute_command($query)
    {
        $attempts_to_connect = 5;
        $attempts_to_execute = 5;
        $sleeping_time = 2;

        if (!($socket = socket_create(AF_INET, SOCK_STREAM, 0)))
        {
            while (!($socket = socket_create(AF_INET, SOCK_STREAM, 0)))
            {
                $attempts_to_connect--;

                sleep($sleeping_time);
            }

            if (!$socket)
            {
                socket_strerror(socket_last_error());

                return;
            }
        }

        $attempts_to_connect = 5;

        if (!(socket_connect($socket, $this->server_ip, $this->server_port)))
        {
            while (!($connect_result = socket_connect($socket, $this->server_ip, $this->server_port)))
            {
                $attempts_to_connect--;

                sleep($sleeping_time);
            }
        }

        $query = sprintf("%d\n" . $query, strlen($query));

        socket_write($socket, $query, strlen($query));

        $response = "";

        do
        {
            while($out = socket_read($socket, 2048))
            {
                $response .= $out;
            }

            $attempts_to_execute--;
        }
        while (empty($response) || $attempts_to_execute != 0);

        return explode("\n", $response);
    }

    private function throw_irbis64_exception($error_code)
    {
        throw new Irbis64_exception("IRBIS64 ERROR: ".$this->IRBIS64_errors[intval($error_code)]);
    }
}