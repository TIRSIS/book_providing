<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Book_model extends CI_Model
{
    private $emulator;

    public function __construct()
    {
        parent::__construct();

        $this->emulator = $this->load->database('emulator', TRUE);
    }

    public function get_books($author, $title, $year, $source = NULL)
    {
        $this->emulator
            ->select('
                bk.id_books AS book_id,
                bk.autor_books AS book_author,
                bk.name_books AS book_title,
                bk.year_books AS book_year', FALSE);

        if($author != NULL)
            $this->emulator->like('bk.autor_books', $author, 'both');

        if($title != NULL)
            $this->emulator->like('bk.name_books', $title, 'both');

        if($year != NULL)
            $this->emulator->like('bk.year_books', $year, 'both');

        return $this->emulator->get('books AS bk')->result_array();
    }
}