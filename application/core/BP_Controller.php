<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BP_Controller extends CI_Controller
{
    protected function check_user()
    {
        $this->CI =& get_instance();

        if(!$this->get_user())
        {
            redirect(site_url().'/Login');
        }
    }

    private function get_user()
    {
        $success = TRUE;

        $session = $this->CI->session->userdata('userinfo');

        $this->CI->session->unset_userdata('userinfo');

        if(empty($session))
        {
            $success = FALSE;
        }
        else
        {
            $waiting_time = '00:30:00';

            $current_datetime = strtotime(date("Y-m-d H:i:s"));
            $registered_datetime = strtotime($session['reg_time']);

            if(($current_datetime - $registered_datetime) >= strtotime($waiting_time))
            {
                $success = FALSE;
            }
            else
            {
                $session['datetime'] = date("Y-m-d H:i:s");

                $this->CI->session->set_userdata(array('userinfo' => $session));
            }
        }

        return $success;
    }
}